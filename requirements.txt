Faker==19.2.0
pathspec==0.11.1
platformdirs==3.8.1
pycodestyle==2.10.0
pyflakes==3.0.1
python-dateutil==2.8.2
six==1.16.0
